# Overview
When working with the Teensy series of development boards I've merged information from multiple sources into combined documents to help my own design process. 

Additional details are found in the [project wiki](https://gitlab.com/SmokingCircuits/TeensyReference/wikis/home).

If you find an error or have a suggestion, please open an issue request so we all can benefit. Thanks!

